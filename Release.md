1. Edit `setup.py` and update at least the version number. Then commit.

2. Checkout the master branch and merge develop:
```
git checkout master
git merge develop
``` 

3. Create a new tag:
```
git tag -a v1.4 -m "my version 1.4"
```

4. Push code to the upstream repository:
```
git push origin master develop --tags
```

5. Create universal binary distribution (wheel):
```
python3 setup.py bdist_wheel
```

6. Upload package (requires credential file ~/pypirc)
```
twine upload dist/* --skip-existing
```

7. Return to develop branch:
```
git checkout develop
```
